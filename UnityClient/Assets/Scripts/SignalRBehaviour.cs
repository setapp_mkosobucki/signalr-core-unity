﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Microsoft.AspNetCore.SignalR.Client;


public class SignalRBehaviour : MonoBehaviour {

    HubConnection _connection;

    [SerializeField]
    Text _textObject;

    string log;

    // Use this for initialization
    void Awake ()
    {
        Debug.Log("Entering SignalR init");
        _connection = new HubConnectionBuilder()
         .WithUrl("http://10.117.0.111:5000/chat")
         .Build();

        _connection.On<string, string>("broadcastMessage", (name, message) => { 
            log += $"{name}: {message}\n";
            Debug.Log($"{name}: {message}");
        });
    }

    private async void Start()
    {
        Debug.Log("Establishing SignalR connection");
        await _connection.StartAsync();
        await _connection.InvokeAsync("Send", "UnityApp", "is now connected");
    }

    async void Update()
    {
        _textObject.text = log;
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended )
        {
            Debug.Log("touch");
            await _connection?.SendAsync("Send", "UnityApp", "sends stuff");
        }
    }

}
